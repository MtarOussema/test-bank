package com.banckaccount.service;

import java.math.BigDecimal;

import com.banckaccount.domain.BankAccount;

public class BankingService {
	public void deposit(BankAccount account, BigDecimal amount) {
		account.deposit(amount);
	}

	public void withdraw(BankAccount account, BigDecimal amount) {
		account.withdraw(amount);
	}

	public void transfer(BankAccount fromAccount, BankAccount toAccount, BigDecimal amount) {
		fromAccount.withdraw(amount);
		toAccount.deposit(amount);
	}
}
