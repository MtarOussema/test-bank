package com.banckaccount.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BankAccount {
	 private BigDecimal balance = BigDecimal.ZERO;
	    private final List<Transaction> transactions = new ArrayList<Transaction>();

	    public void deposit(BigDecimal amount) {
	        if (amount.compareTo(BigDecimal.ZERO) < 0) {
	            throw new IllegalArgumentException("Deposit amount must be positive");
	        }

	        balance = balance.add(amount);
	        transactions.add(new Transaction(LocalDateTime.now(), amount, balance));
	    }

	    public void withdraw(BigDecimal amount) {
	        if (amount.compareTo(BigDecimal.ZERO) < 0) {
	            throw new IllegalArgumentException("Withdrawal amount must be positive");
	        }
	        if (amount.compareTo(balance) > 0) {
	            throw new IllegalArgumentException("Not enough balance");
	        }

	        balance = balance.subtract(amount);
	        transactions.add(new Transaction(LocalDateTime.now(), amount.negate(), balance));
	    }

	    public List<Transaction> getStatement() {
	        return Collections.unmodifiableList(transactions);
	    }

	    public BigDecimal getBalance() {
	        return balance;
	    }
}
