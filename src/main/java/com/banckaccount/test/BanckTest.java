package com.banckaccount.test;


import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.banckaccount.domain.BankAccount;
import com.banckaccount.domain.Transaction;
public class BanckTest {
	   private BankAccount account;

	    @Before
	    public void setup() {
	        account = new BankAccount();
	    }

	    @Test
	    public void testDeposit() {
	        account.deposit(BigDecimal.TEN);
	        assertEquals(BigDecimal.TEN, account.getBalance());
	    }

	    @Test
	    public void testWithdraw() {
	        account.deposit(BigDecimal.TEN);
	        account.withdraw(BigDecimal.ONE);
	        assertEquals(BigDecimal.valueOf(9), account.getBalance());
	    }

	    @Test
	    public void testGetStatement() {
	        account.deposit(BigDecimal.TEN);
	        account.withdraw(BigDecimal.ONE);
	        List<Transaction> statement = account.getStatement();

	        assertEquals(2, statement.size());
	        assertEquals(BigDecimal.TEN, statement.get(0).getAmount());
	        assertEquals(BigDecimal.ONE.negate(), statement.get(1).getAmount());
	    }

	    @Test(expected = IllegalArgumentException.class)
	    public void testNegativeDeposit() {
	        account.deposit(BigDecimal.ONE.negate());
	    }

	    @Test(expected = IllegalArgumentException.class)
	    public void testNegativeWithdraw() {
	        account.withdraw(BigDecimal.ONE.negate());
	    }

	    @Test(expected = IllegalArgumentException.class)
	    public void testWithdrawMoreThanBalance() {
	        account.withdraw(BigDecimal.TEN);
	    }
}
